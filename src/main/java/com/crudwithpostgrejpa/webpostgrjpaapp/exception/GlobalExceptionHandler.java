
package com.crudwithpostgrejpa.webpostgrjpaapp.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

//here to use ErrorDetails to return the error response,lets create a GlobalExceptionHandler class
//annotated by @ControllerAdvice annotation.this class handles exception specific and global exception in a single place



@ControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(ResourceNotFoundException.class)
	public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException ex,WebRequest request){
		ErrorDetails errorDetails = new ErrorDetails(new Date(),ex.getMessage(),request.getDescription(false));//request.getdescription(true) here exceeds other after url eg ....employees/1: client=0:0:0:0:1
		return new ResponseEntity<>(errorDetails,HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
		
		
	

}
